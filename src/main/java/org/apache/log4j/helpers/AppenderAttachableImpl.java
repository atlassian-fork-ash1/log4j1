/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.log4j.helpers;

import org.apache.log4j.Appender;
import org.apache.log4j.spi.AppenderAttachable;
import org.apache.log4j.spi.LoggingEvent;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A straightforward implementation of the {@link AppenderAttachable} interface.
 *
 * @author Ceki G&uuml;lc&uuml;
 * @since version 0.9.1
 */
public
class AppenderAttachableImpl implements AppenderAttachable {

  /**
   * Atlassian performance change to native log4j.  The list of appenders is thread safe and optimised for iterating.
   * New appenders entering the list are very rare after startup and removal even more so.
   * <p/>
   * But iterating (and then writing to) each appender is uber common, and that's right up
   * CopyOnWriteArrayList's alley.
   */
  private final CopyOnWriteArrayList<Appender> appenderList = new CopyOnWriteArrayList<Appender>();

  /**
   * Attach an appender. If the appender is already in the list in won't be added again.
   */
  public
  void addAppender(Appender newAppender) {
    // Null values for newAppender parameter are strictly forbidden.
    if (newAppender == null) {
      return;
    }
    synchronized (appenderList) {
      if (!appenderList.contains(newAppender)) {
        appenderList.add(newAppender);
      }
    }
  }

  /**
   * Call the <code>doAppend</code> method on all attached appenders.
   */
  public
  int appendLoopOnAppenders(LoggingEvent event) {
    int size = appenderList.size();
    for (Appender appender : appenderList) {
      appender.doAppend(event);
    }
    return size;
  }

  /**
   * Enumeration for old school reasons
   */
  private static
  class EnumerationFromIter implements Enumeration {
    final Iterator iterator;

    private
    EnumerationFromIter(final Iterable iterable) {
      this.iterator = iterable.iterator();
    }

    public
    boolean hasMoreElements() {
      return iterator.hasNext();
    }

    public
    Object nextElement() {
      return iterator.next();
    }
  }


  /**
   * Get all attached appenders as an Enumeration.
   *
   * @return Enumeration An enumeration of attached appenders.
   */
  public
  Enumeration getAllAppenders() {
    return new EnumerationFromIter(appenderList);
  }

  /**
   * Look for an attached appender named as <code>name</code>.
   * <p/>
   * <p>Return the appender with that name if in the list. Return null otherwise.
   */
  public
  Appender getAppender(String name) {
    if (name == null) {
      return null;
    }
    for (Appender appender : appenderList) {
      if (name.equals(appender.getName())) {
        return appender;
      }
    }
    return null;
  }


  /**
   * Returns <code>true</code> if the specified appender is in the list of attached appenders, <code>false</code>
   * otherwise.
   *
   * @since 1.2
   */
  public
  boolean isAttached(Appender appender) {
    if (appender == null) {
      return false;
    }

    for (Appender a : appenderList) {
      if (a == appender) {
        return true;
      }
    }
    return false;
  }


  /**
   * Remove and close all previously attached appenders.
   */
  public
  void removeAllAppenders() {
    for (Appender appender : appenderList) {
      appender.close();
    }
    appenderList.clear();
  }


  /**
   * Remove the appender passed as parameter form the list of attached appenders.
   */
  public
  void removeAppender(Appender appender) {
    if (appender == null) {
      return;
    }
    appenderList.remove(appender);
  }


  /**
   * Remove the appender with the name passed as parameter form the list of appenders.
   */
  public
  void removeAppender(String name) {
    if (name == null) {
      return;
    }
    for (Appender appender : appenderList) {
      if (name.equals(appender.getName())) {
        appenderList.remove(appender);
        break;
      }
    }
  }
}
